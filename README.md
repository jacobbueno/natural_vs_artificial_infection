# NATURAL_VS_ARTIFICIAL_INFECTION


First round of work comparing the infected EMIT influenza H3 Donors with the naturally infected influenza H3 cases from the 2012-2013 sampling campaign at University of Maryland that enrolled cases based on fever and cough/sore throat or QuickVue rapid antigen test. Scripts from this work follow:

_Natural_vs_Artificial_Analysis.R_ = Use to perform main analyses except for propensity scores -- final PS models in the _Natural_vs_Artificial_Analysis_PS_Models.R_ script -- although this _Natural_vs_Artificial_Analysis.R_ script does contain draft PS material (please refer to the _Natural_vs_Artificial_Analysis_PS_Models.R_ script for the final models)

_EMIT_ever_febrile_cases.R_ = Use to clarify analyses related to febrile illnesses. 

_Natural_vs_Artificial_Analysis_PS_Models.R_ = use to achieve final PS models used in the manuscript (MODEL 13 was evaluated to be the "best" model, although it failed to warrant further comparison of the study population groups, showing that the groups were simply too different to make any sort of subset comparisons that could afford comparable comparison, at least by using symptom variables to drive PS exploration.)

_Natural_vs_Artificial_Analysis_PS_Models_Shedding_Risk_Outcome.R_ = Contains draft material to begin looking at doing PS models with the outcome of shedding risk (as opposed to shedding quantity as was the focus in _Natural_vs_Artificial_Analysis_PS_Models.R_)

_vaccination_exp_vs_emit_natural.R_ = use to investigate the the vaccination variables in the EMIT naturally infected dataset (2012-2013 cases recruited mostly from University Health Center). Tables 1 and 2 were updated as a result. 

Note that some tweaks to Table 2 (aside from vaccination analysis) was done in _vaccination_exp_vs_emit_natural.R_. Review of this script and tweaking of Table 2 is required to reproducibility. 



New work looking at making additional comparisons with the EMIT Donors and EMIT 2012-13 cases with the Prometheus influenza cases that aren't selected with such bias toward the most severely symptomatic. 

_Prepare_Prometheus_Data.R_ = Use to prepare Prometheus data as another comparison group.

_Experimental_vs_Prometheus_Clean_Cough.R_ = Use to prepare cough data

_Experimental_vs_Prometheus.R_ = Use to do analysis comparing the experimental EMIT cases with the mildly symptomatic ones from Prometheus. May included comparison with the EMIT naturally infected population (2012-2013) as well at some point. 

_prometheus_experimenal_natural_infections.R_ =  Use to begin exploring comparisons from EMIT experimental cases, EMIT naturally infected cases (2012-2013 recruited from University Health Center with fever and cough/sore throat or QuickVue rapid test positive) and Prometheus naturally infected cases (generally mild infections not recruited from University Health Center)

