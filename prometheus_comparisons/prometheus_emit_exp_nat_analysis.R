# prometheus_experimental_natural_infections.R
# Adding Prometheus data to the analysis.


# Compare prometheus infections with experimental infections

# According to summary of qRT-PCR data:
# IAV aerosol samples tested (fine): 5
# IAV aerosol samples positive (fine): 33
# IBV aerosol samples tested (fine): 2
# IBV aerosol samples positive (fine): 8




# Numerator is the fine aerosol positive fraction
fisher.test(matrix(c(7, 41-7, 11, 42-11), ncol = 2))

# restricted to fluA samples
fisher.test(matrix(c(5, 33-5, 11, 42-11), ncol = 2))


# # If numerator is the fine and coarse aerosol positive case fraction...
# fisher.test(matrix(c(7, 41-7, 11, 42-11), ncol = 2))



# Compare prometheus infections with natural infections
# restricted to flu A samples
fisher.test(matrix(c(5, 33-5, 71, 83-71), ncol = 2))

# Compare experimental infections with natural infections

fisher.test(matrix(c(11, 42-11, 71, 83-71), ncol = 2))


# Prometheus viral aerosol shedding detected per sample (per 30 min G-II sample):

# Y3

y3_coarse <- c(1.15E3, 5.7E2, 3.67E3, 1.68E3)
y3_fine <- c(1.18E5, 1.67E3, 7.39E3)

y2_coarse <- c(1.38E4)
y2_fine <- c(2.43E4, 5.54E2)

y123_fine <- c(2.43E4, 5.54E2, 1.18E5, 1.67E3, 7.39E3)

y123_fine_mean <- mean(y123_fine)
y123_fine_mean
y123_fine_sd <- sd(y123_fine)
y123_fine_sd

y123_fine_geom_mean <- exp(mean(log(y123_fine)))
y123_fine_geom_mean
y123_fine_geom_sd <- exp(sd(log(y123_fine)))
y123_fine_geom_sd

# comparing the GM (gsd) for Prometheus with EMIT Donors

# EMIT_donors: The geometric means (GM) and geometric standard deviations (GSD) over all positive samples were 3.14E+3 (3.33) and 5.31E+3 (4.59) for coarse (N=6) and fine (N=14) aerosol samples, respectively.

donor_emit_fine <- rnorm(14,
                         mean = log(5.31E+3), 
                         sd = log(4.59))

prometheus_umd_fine <- rnorm(5,
                             mean = log(y123_fine_geom_mean),
                             sd = log(y123_fine_geom_sd))

t.test(donor_emit_fine, prometheus_umd_fine, var.equal = FALSE)
t.test(donor_emit_fine, prometheus_umd_fine, var.equal = FALSE)$p.value












y3_coarse_avg <- mean(y3_coarse)
y3_coarse_avg


