* Script: Artificial_vs_Natural_Infection_regression.SAS;
* Author: Jacob Bueno de Mesquita;
* Date: March 2019
;

* Summary: This script imports dataset (produced with R script in the NATURAL_VS_ARTIFICIAL_INFECTION project)
* 1) Imputes cough number for UK study instances where cough number not recorded. 
* 2) Imputes cough number for UMD study instances where cough number not recorded. 
* 3) Tobit on UK fine 28 replicates from 14 positive samples (with 4 NA replicates) to impute values for the negative replicates.
* 4) Tobit on UK coarse 12 replicates from 6 positive samples (with 4 NA replicates) to impute values for the negative replicates.
* 5) Tobit on UMD fine 226 replicates from ?# positive samples (with 26 NA replicates) to impute values for the negative replicates.
* 6) Tobit on UMD coarsee 134 replicates from ?# positive samples (with 22 NA replicates) to impute values for the negative replicates.
;


*** 1) ***
;

* Import the data;
FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UK_39_pcr_pos_for_cough_imputation.csv';

PROC IMPORT 
	OUT = UK_cough_imputation
	DATAFILE = REFFILE
	DBMS = CSV REPLACE;
	GETNAMES = YES;
RUN;

PROC CONTENTS 
	data = UK_cough_imputation;
RUN;

data UK_cough_imputation; 
	set UK_cough_imputation;
	cough_number_numeric = input(cough_number, 3.0);
	Ct_numeric = input(Ct, 5.2);
	subject_id_char = put(subject.id, 3.);
	study_day_char = put(study.day, 1.);
RUN;

* The mixed models below struggle to incorporate the single observed cough value that is able 30 into the model, perhaps taking this out will improve fit, but is it valid to do this?;
* Upon trying this, we see that dramatic improvement in model fit is seen. Thus we have the question perhaps about the tradeoff between model fit and model validity. ;

*data UK_cough_imputation;
*	set UK_cough_imputation;
	
*if cough_number_numeric > 30  then delete;
		
*RUN;	

* Manipulate the study.day variable to get dummy variables. ;
data UK_cough_imputation;
	set UK_cough_imputation;

	if study.day = 2 then
		X1 = 1;
	else
		X1 = 0;

	if study.day = 3 then
		X2 = 1;
	else
		X2 = 0;
		
	if study.day = 4 then
		X3 = 1;
	else
		X3 = 0;
run;
quit;



* Apply mixed model with fixed effect cough and study day and sex and random effect of person;
* Trying two methods here  and comparing their residuals to choose the best model


* first model: mixed model using reml with fixed effects of cough symptom, study day, and sex and random effect of person;

ods graphics on;
PROC MIXED DATA = UK_cough_imputation NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char study_day_char sex;
	MODEL cough_number_numeric = cough study_day_char sex /  S CHISQ influence residual outp = work.uk_cough_model_mixed ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
ods graphics off;
* Only used this model in the final results. The other models (below) were exploratory and didn't have any greater fit than this one. 


* second model: mixed model using reml with fixed effects of cough symptom, study day, and random effect of person ;

ods graphics on;
PROC MIXED DATA = UK_cough_imputation NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char study_day_char ;
	MODEL cough_number_numeric = cough study_day_char  /  S CHISQ influence residual outp = work.uk_cough_model_mixed_2 ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
ods graphics off;


* third model: mixed model using reml with fixed effects of cough symptom, and random effect of person ;

ods graphics on;
PROC MIXED DATA = UK_cough_imputation NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char  ;
	MODEL cough_number_numeric = cough   /  S CHISQ influence residual outp = work.uk_cough_model_mixed_3 ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
ods graphics off;




* However, some predicted values go below 0, which we isn't true because 0 is the lowest value possible;
* Trying this same model in glimmix and using a poisson distribution and log link might help (model would not converge uging identity link);

PROC GLIMMIX DATA = UK_cough_imputation noclprint = 100 method = RSPL;
	CLASS subject_id_char X1 X2 X3 sex;
	MODEL cough_number_numeric = X1 X2 X3 sex / DIST = poisson LINK = log S ddfm=satterth intercept solution;
	RANDOM INTERCEPT / sub = subject_id_char;
	output out = uk_cough_model_glimmix pred = model_predicted_value resid = model_residual;
RUN;


* Try using zero-inflated poisson approach with glimmix;

proc nlmixed data = UK_cough_imputation XTOL = 1E-12 method = GAUSS qpoints = 100;
   parameters b0 = 0 b1 = 0 b2 = 0 b3 = 0 b4 = 0 b5 = 0
              a0 = 0 a1 = 0 ;
   /* linear predictor for the inflation probability      */
   linpinfl = a0 + a1*subject_id_char;
   /* infprob = inflation probability for zeros           */
   /*         = logistic transform of the linear predictor*/
   infprob  = 1/(1+exp(-linpinfl));
   /* Poisson mean */
   lambda   = exp(b0 + b1*cough + b2*X1 + b3*X2 + b4*X3 + b5*sex );
   /* Build the ZIP log likelihood */
   if cough_number_numeric = 0 then 
        ll = log(infprob + (1-infprob)*exp(-lambda));
   else ll = log((1-infprob)) - lambda  + cough_number_numeric*log(lambda) - lgamma(cough_number_numeric + 1);
   model cough_number_numeric ~ general(ll);
   predict lambda out = uk_cough_zero_infl_poisson;
run;
* This didn't work;



* Try using zero-inflated negative binomial approach with glimmix;

proc nlmixed data = UK_cough_imputation;
   parameters b0 = 0 b1 = 0 b2 = 0 b3 = 0
              a0 = 0 a1 = 0 alpha = 1;
   /* linear predictor for the inflation probability      */
   linpinfl = a0 + a1*subject_id_char;
   /* infprob = inflation probability for zeros           */
   /*         = logistic transform of the linear predictor*/
   infprob  = 1/(1+exp(-linpinfl));
   /* negative binomial with mean-dispersion */
   lambda   = exp(b0 + b1*cough + b2*X1 + b3*X2 + b4*X3 + b5*sex );
   /* Build the ZIP log likelihood */
   m = 1/alpha;
   p = 1/(1+alpha*lambda);
   if cough_number_numeric = 0 then 
        ll = log(infprob + (1-infprob)*(p**m));
   else ll = log(1-infprob) + log(gamma(m + cough_number_numeric)) - log(gamma(cough_number_numeric + 1))
   			 - log(gamma(m)) + m*log(p) + cough_number_numeric*log(1-p);
   model cough_number_numeric ~ general(ll);
run;




*** 2) ***
;

***** working with UMD data ******
* read in UMD data for tobit regrssion. will get predicted values for logfinal copies shedding,
*  which will be useful for predicting cough count in the instances where it was not observed. ;

%web_drop_table(WORK.UMD_83_flu);


FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_83_H3_cases_for_tobit_then_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_83_flu;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_83_flu; RUN;


%web_open_table(WORK.UMD_83_flu);


data UMD_83_flu; 
	set UMD_83_flu;
	subject_id_char = put(subject.id, 3.);
	study_day_char = put(dpo, 1.);
RUN;


data UMD_83_flu;
	set UMD_83_flu;
	
	if final.copies ~=. then
		logfinalcopies=log10(final.copies);

	if final.copies =. and type='A' then
		logfinalcopies=log10(2000);

	if final.copies =. and type='B' then
		logfinalcopies=log10(9000);

run;



data finetotal;
	set UMD_83_flu;

	if dpo=2 then
		X1=1;
	else
		X1=0;

	if dpo=3 then
		X2=1;
	else
		X2=0;
run;
quit;


* trying first model - simply fixed effects of study day and random effect of person ;

proc genmod data = finetotal;
	class subject_id_char ;
	model logfinalcopies = X1 X2 /dist=poisson;
	repeated subject=subject_id_char/ printmle;
	ods OUTPUT parameterestimates=params;
run;

data Params;
	set Params (keep=Parameter Estimate);
run;

proc transpose data=Params out=Paramst;
run;

data _null_;
	set Paramst;
	call symput('unfBeta0', COL1);
	call symput('unfBeta1', COL2);
	call symput('unfBeta2', COL3);
run;



proc nlmixed data=finetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of fine Particles: Effect of dpo2 dpo3";
	parms sigma2_u=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1 beta2=&unfBeta2;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j + beta1*X1 + beta2*X2 ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = umd_finalcopies_for_cough_model;
run;
quit;


* trying second model - fixed effects of study day and BMI and random effect of person ;

proc genmod data = finetotal;
	class subject_id_char ;
	model logfinalcopies = X1 X2 BMI /dist=poisson;
	repeated subject=subject_id_char/ printmle;
	ods OUTPUT parameterestimates=params;
run;

data Params;
	set Params (keep=Parameter Estimate);
run;

proc transpose data=Params out=Paramst;
run;

data _null_;
	set Paramst;
	call symput('unfBeta0', COL1);
	call symput('unfBeta1', COL2);
	call symput('unfBeta2', COL3);
	call symput('unfBeta3', COL4);
run;



proc nlmixed data=finetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of fine Particles: Effect of dpo2 dpo3";
	parms sigma2_u=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1 beta2=&unfBeta2 beta3 = &unfBeta3;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j + beta1*X1 + beta2*X2 + beta3*BMI ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = umd_finalcopies_for_cough_modelb;
run;
quit;

* Adding BMI does seem to improve AIC but only by a very small margin, thus it may be worth sticking with the basic model for now and ignoring BMI.
* Taking the simply model to impute the RNA copies for fine aerosols, for now. ;
* Using the Pred (predicted RNA copies for fine aerosol) in model to predict cough count next. 

* Importing this output into R and exploring it some, taking the replicate average, and reading in the resulting dataset (UMD_data_for_cough_imputation).;



* Model 1: Mixed model with fixed effects of cough, loginalcopies, sex, age, BMI, dpo and rand effect of person;
* ... to predict umd population cough_number (need predicted values for those without cough);

FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_data_for_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_cough_imp_mixedmod_data;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_cough_imp_mixedmod_data; RUN;


PROC MIXED DATA = Umd_cough_imp_mixedmod_data NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char study_day_char sex;
	MODEL cough_number = cough Pred study_day_char age BMI sex /  S CHISQ influence residual outp = work.umd_cough_model_mixed ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
quit;



* Model 2: Mixed model with fixed effects of cough, loginalcopies, dpo, and sex, and rand effect of person;
* ... to predict umd population cough_number (need predicted values for those without cough);

FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_data_for_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_cough_imp_mixedmod_data;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_cough_imp_mixedmod_data; RUN;


PROC MIXED DATA = Umd_cough_imp_mixedmod_data NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char study_day_char sex;
	MODEL cough_number = cough Pred study_day_char sex /  S CHISQ influence residual outp = work.umd_cough_model_mixed_2 ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
quit;



* Model 3: Mixed model with fixed effects of cough, loginalcopies, dpo, and rand effect of person;
* ... to predict umd population cough_number (need predicted values for those without cough);

FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_data_for_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_cough_imp_mixedmod_data;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_cough_imp_mixedmod_data; RUN;


PROC MIXED DATA = Umd_cough_imp_mixedmod_data NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char study_day_char ;
	MODEL cough_number = cough Pred study_day_char  /  S CHISQ influence residual outp = work.umd_cough_model_mixed_3 ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
quit;



* Model 4: Mixed model with fixed effects of cough, loginalcopies, and sex, and rand effect of person;
* ... to predict umd population cough_number (need predicted values for those without cough);

FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_data_for_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_cough_imp_mixedmod_data;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_cough_imp_mixedmod_data; RUN;


PROC MIXED DATA = Umd_cough_imp_mixedmod_data NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char sex;
	MODEL cough_number = cough Pred sex /  S CHISQ influence residual outp = work.umd_cough_model_mixed_4 ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
quit;



* Model 5: Mixed model with fixed effects of cough, loginalcopies, and rand effect of person;
* ... to predict umd population cough_number (need predicted values for those without cough);

FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_data_for_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_cough_imp_mixedmod_data;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_cough_imp_mixedmod_data; RUN;


PROC MIXED DATA = Umd_cough_imp_mixedmod_data NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char ;
	MODEL cough_number = cough Pred /  S CHISQ influence residual outp = work.umd_cough_model_mixed_5 ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
quit;



* Model 6: Mixed model with fixed effects of cough, and rand effect of person;
* ... to predict umd population cough_number (need predicted values for those without cough);

FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_data_for_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_cough_imp_mixedmod_data;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_cough_imp_mixedmod_data; RUN;


PROC MIXED DATA = Umd_cough_imp_mixedmod_data NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char ;
	MODEL cough_number = cough /  S CHISQ influence residual outp = work.umd_cough_model_mixed_6 ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
quit;



* Model 7: Mixed model with fixed effects of cough, and rand effect of person;
* ... to predict umd population cough_number (need predicted values for those without cough);

FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_data_for_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_cough_imp_mixedmod_data;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_cough_imp_mixedmod_data; RUN;


PROC MIXED DATA = Umd_cough_imp_mixedmod_data NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char ;
	MODEL cough_number = Pred /  S CHISQ influence residual outp = work.umd_cough_model_mixed_7 ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
quit;



****************************************************************
**** Creating Table 2 -- looking at just the positive samples;
** Need to use tobit here to get the values for replicates below limit of detection from positive samples;
* Question is whether to add any fixed effects into these models or just run with the intercept and the random effect of person? ;
* Could run both ways and see which model fits better and use that one?
;


*** 3) ***
;

** start with UK fine **;


FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UK_fine_pos_samples.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UK_fine_pos_samples;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UK_fine_pos_samples; RUN;


%web_open_table(WORK.UK_fine_pos_samples);


data UK_fine_pos_samples; 
	set UK_fine_pos_samples;
	subject_id_char = put(subject.id, 3.);
RUN;


data UK_fine_pos_samples;
	set UK_fine_pos_samples;
	
	if final.copies ~=. then
		logfinalcopies=log10(final.copies);

	if final.copies =. and type='A' then
		logfinalcopies=log10(2000);

	if final.copies =. and type='B' then
		logfinalcopies=log10(9000);

run;



data finetotal;
	set UK_fine_pos_samples;

	if study.day=3 then
		X1=1;
	else
		X1=0;

	if study.day=4 then
		X2=1;
	else
		X2=0;

run;
quit;



proc genmod data = finetotal;
	class subject_id_char ;
	model logfinalcopies = cough_count_full X1 X2 /dist=poisson;
	repeated subject=subject_id_char/ printmle;
	ods OUTPUT parameterestimates=params;
run;

data Params;
	set Params (keep=Parameter Estimate);
run;

proc transpose data=Params out=Paramst;
run;

data _null_;
	set Paramst;
	call symput('unfBeta0', COL1);
	call symput('unfBeta1', COL2);
	call symput('unfBeta2', COL3);
	call symput('unfBeta3', COL4);
run;



proc nlmixed data=finetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of fine Particles: Effect of cough count, dpo2, dpo3";
	parms sigma2_u=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1 beta2=&unfBeta2 beta3=&unfBeta3;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j + beta1*cough_count_full + beta2*X1 + beta3*X2 ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = uk_fine_pos_tobit_cough_day ;
run;
quit;

proc nlmixed data=finetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of fine Particles: intercept model with random effect of person";
	parms sigma2_u=1 sigma2=1 beta0=0;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu = beta0 + b_0j ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = uk_fine_pos_tobit_intercept ;
run;
quit;

* The more complex model worked a little better (lower AIC!)



*** 4) ***
;

******
** Now model for UK coarse ;


FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UK_coarse_pos_samples.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UK_coarse_pos_samples;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UK_coarse_pos_samples; RUN;


%web_open_table(WORK.UK_coarse_pos_samples);


data UK_coarse_pos_samples; 
	set UK_coarse_pos_samples;
	subject_id_char = put(subject.id, 3.);
	final_copies_numeric = input(final.copies, 16.10);
RUN;


data UK_coarse_pos_samples;
	set UK_coarse_pos_samples;
	
	if final_copies_numeric ~=. then
		logfinalcopies=log10(final_copies_numeric);

	if final_copies_numeric =. and type='A' then
		logfinalcopies=log10(2000);

run;

data UK_coarse_pos_samples; 
	set UK_coarse_pos_samples;
	logfinalcopies_numeric = input(logfinalcopies, 12.4);
RUN;

data coarsetotal;
	set UK_coarse_pos_samples;

	if study.day=2 then
		X1=1;
	else
		X1=0;

	if study.day=3 then
		X2=1;
	else
		X2=0;
run;
quit;


proc genmod data = coarsetotal;
	class subject_id_char ;
	model logfinalcopies_numeric = cough_count_full X1 X2 /dist=poisson;
	repeated subject=subject_id_char/ printmle;
	ods OUTPUT parameterestimates=params;
run;

data Params;
	set Params (keep=Parameter Estimate);
run;

proc transpose data=Params out=Paramst;
run;

data _null_;
	set Paramst;
	call symput('unfBeta0', COL1);
	call symput('unfBeta1', COL2);
	call symput('unfBeta2', COL3);
	call symput('unfBeta3', COL4);
run;



proc nlmixed data=coarsetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of coarse Particles: Effect of cough count, dpo2";
	parms sigma2_u=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1 beta2=&unfBeta2 beta3=&unfBeta3;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j + beta1*cough_count_full + beta2*X1 + beta3*X2;

	if final_copies_numeric ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies_numeric - mu)**2 / (2*sigma2) );

	if final_copies_numeric =.  then
		ll=probnorm((logfinalcopies_numeric - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies_numeric~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = uk_coarse_pos_tobit_cough_day ;
run;
quit;

proc nlmixed data=coarsetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of coarse Particles: intercept model with random effect of person";
	parms sigma2_u=1 sigma2=1 beta0=0;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu = beta0 + b_0j ;

	if final_copies_numeric ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies_numeric - mu)**2 / (2*sigma2) );

	if final_copies_numeric =.  then
		ll=probnorm((logfinalcopies_numeric - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies_numeric~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = uk_coarse_pos_tobit_intercept ;
run;
quit;
* The less complex model worked a little better (lower AIC!)



*** 5) ***
;

******
Now UMD fine model ;

* Should probably try with and without the nested random effect. This first iteration will be done without nested random effect of sample within subject. 
;


FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_fine_pos_samples.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_fine_pos_samples;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_fine_pos_samples; RUN;


%web_open_table(WORK.UMD_fine_pos_samples);


data UMD_fine_pos_samples; 
	set UMD_fine_pos_samples;
	subject_id_char = put(subject.id, 3.);
RUN;


data UMD_fine_pos_samples;
	set UMD_fine_pos_samples;
	
	if final.copies ~=. then
		logfinalcopies=log10(final.copies);

	if final.copies =. and type='A' then
		logfinalcopies=log10(2000);

	if final.copies =. and type='B' then
		logfinalcopies=log10(9000);

run;



data finetotal;
	set UMD_fine_pos_samples;

	if study.day=2 then
		X1=1;
	else
		X1=0;

	if study.day=3 then
		X2=1;
	else
		X2=0;
run;
quit;



proc genmod data = finetotal;
	class subject_id_char ;
	model logfinalcopies = cough_count_full X1 X2 /dist=poisson;
	repeated subject=subject_id_char/ printmle;
	ods OUTPUT parameterestimates=params;
run;

data Params;
	set Params (keep=Parameter Estimate);
run;

proc transpose data=Params out=Paramst;
run;

data _null_;
	set Paramst;
	call symput('unfBeta0', COL1);
	call symput('unfBeta1', COL2);
	call symput('unfBeta2', COL3);
	call symput('unfBeta3', COL4);
run;



proc nlmixed data=finetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of fine Particles: Effect of cough count, dpo2, dpo3";
	parms sigma2_u=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1 beta2=&unfBeta2 beta3=&unfBeta3;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j + beta1*cough_count_full + beta2*X1 + beta3*X2 ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = umd_fine_pos_tobit_cough_day ;
run;
quit;


proc nlmixed data=finetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of fine Particles: Effect of cough count";
	parms sigma2_u=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j + beta1*cough_count_full ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = umd_fine_pos_tobit_cough ;
run;
quit;


proc nlmixed data=finetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of fine Particles: intercept model with random effect of person";
	parms sigma2_u=1 sigma2=1 beta0=0;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu = beta0 + b_0j ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = umd_fine_pos_tobit_intercept ;
run;
quit;


* The most complex model (fixed effects of cough_count and study day with rand effect person) was best (lower AIC)



*** 6) ***
;

***
**** Now UMD coarse;

FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UMD_coarse_pos_samples.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_coarse_pos_samples;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_coarse_pos_samples; RUN;


%web_open_table(WORK.UMD_coarse_pos_samples);


data UMD_coarse_pos_samples; 
	set UMD_coarse_pos_samples;
	subject_id_char = put(subject.id, 3.);
RUN;


data UMD_coarse_pos_samples;
	set UMD_coarse_pos_samples;
	
	if final.copies ~=. then
		logfinalcopies=log10(final.copies);

	if final.copies =. and type='A' then
		logfinalcopies=log10(2000);

	if final.copies =. and type='B' then
		logfinalcopies=log10(9000);

run;



data coarsetotal;
	set UMD_coarse_pos_samples;

	if study.day=2 then
		X1=1;
	else
		X1=0;

	if study.day=3 then
		X2=1;
	else
		X2=0;
run;
quit;



proc genmod data = coarsetotal;
	class subject_id_char ;
	model logfinalcopies = cough_count_full X1 X2 /dist=poisson;
	repeated subject=subject_id_char/ printmle;
	ods OUTPUT parameterestimates=params;
run;

data Params;
	set Params (keep=Parameter Estimate);
run;

proc transpose data=Params out=Paramst;
run;

data _null_;
	set Paramst;
	call symput('unfBeta0', COL1);
	call symput('unfBeta1', COL2);
	call symput('unfBeta2', COL3);
	call symput('unfBeta3', COL4);
run;



proc nlmixed data=coarsetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of coarse Particles: Effect of cough count, dpo2, dpo3, with random effect of person";
	parms sigma2_u=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1 beta2=&unfBeta2 beta3=&unfBeta3;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j + beta1*cough_count_full + beta2*X1 + beta3*X2 ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = umd_coarse_pos_tobit_cough_day ;
run;
quit;



proc nlmixed data=coarsetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of coarse Particles: Effect of cough count, with random effect of person";
	parms sigma2_u=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1 ;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j + beta1*cough_count_full ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = umd_coarse_pos_tobit_cough ;
run;
quit;



proc nlmixed data=coarsetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of coarse Particles: intercept model with random effect of person";
	parms sigma2_u=1 sigma2=1 beta0=0;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu = beta0 + b_0j ;

	if final.copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final.copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char;
	predict mu out = umd_coarse_pos_tobit_intercept ;
run;
quit;


* The most complex model (fixed effects of cough_count and study day with rand effect person) was best (lower AIC)



