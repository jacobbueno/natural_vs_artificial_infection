* Script: Artificial_vs_Natural_Infection_regression.SAS;
* This script was used with SAS Desktop version because SAS Studio was timing out before models converged. 
* Author: Jacob Bueno de Mesquita;
* Date: March 2019;

* Summary: This script imports dataset (produced with R script in the NATURAL_VS_ARTIFICIAL_INFECTION project)
* 1) Imputes cough number for UK study instances where cough number not recorded. 
* 2) Imputes cough number for UMD study instances where cough number not recorded. 
* 3) 
* 4) 


* Import the data;
FILENAME REFFILE '/home/jbueno/sasuser.v94/Milton Lab/Dissertation/UK_39_pcr_pos_for_cough_imputation.csv';

PROC IMPORT 
	OUT = UK_cough_imputation
	DATAFILE = REFFILE
	DBMS = CSV REPLACE;
	GETNAMES = YES;
RUN;

PROC CONTENTS 
	data = UK_cough_imputation;
RUN;

data UK_cough_imputation; 
	set UK_cough_imputation;
	cough_number_numeric = input(cough_number, 3.0);
	Ct_numeric = input(Ct, 5.2);
	subject_id_char = put(subject.id, 3.);
	study_day_char = put(study.day, 1.);
RUN;


data UK_cough_imputation;
	set UK_cough_imputation;
	
if cough_number_numeric > 30  then delete;
		
RUN;	

* Manipulate the study.day variable to get dummy variables. ;
data UK_cough_imputation;
	set UK_cough_imputation;

	if study.day = 2 then
		X1 = 1;
	else
		X1 = 0;

	if study.day = 3 then
		X2 = 1;
	else
		X2 = 0;
		
	if study.day = 4 then
		X3 = 1;
	else
		X3 = 0;
run;
quit;



* Apply mixed model with fixed effect cough and study day and sex and random effect of person;
* Trying two methods here  and comparing their residuals to choose the best model


* first model: mixed model using reml;

ods graphics on;
PROC MIXED DATA = UK_cough_imputation NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char study_day_char sex;
	MODEL cough_number_numeric = cough study_day_char sex /  S CHISQ influence residual outp = work.uk_cough_model_mixed ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
ods graphics off;
* Only used this model in the final results. The other models were exploratory and didn't have any greater fit than this one. 

* However, some predicted values go below 0, which we isn't true because 0 is the lowest value possible;
* Trying this same model in glimmix and using a poisson distribution and log link might help (model would not converge uging identity link);

PROC GLIMMIX DATA = UK_cough_imputation noclprint = 100 method = RSPL;
	CLASS subject_id_char X1 X2 X3 sex;
	MODEL cough_number_numeric = X1 X2 X3 sex / DIST = poisson LINK = log S ddfm=satterth intercept solution;
	RANDOM INTERCEPT / sub = subject_id_char;
	output out = uk_cough_model_glimmix pred = model_predicted_value resid = model_residual;
RUN;


* Try using zero-inflated poisson approach with glimmix;

proc nlmixed data = UK_cough_imputation XTOL = 1E-12 method = GAUSS qpoints = 100;
   parameters b0 = 0 b1 = 0 b2 = 0 b3 = 0 b4 = 0 b5 = 0
              a0 = 0 a1 = 0 ;
   /* linear predictor for the inflation probability      */
   linpinfl = a0 + a1*subject_id_char;
   /* infprob = inflation probability for zeros           */
   /*         = logistic transform of the linear predictor*/
   infprob  = 1/(1+exp(-linpinfl));
   /* Poisson mean */
   lambda   = exp(b0 + b1*cough + b2*X1 + b3*X2 + b4*X3 + b5*sex );
   /* Build the ZIP log likelihood */
   if cough_number_numeric = 0 then 
        ll = log(infprob + (1-infprob)*exp(-lambda));
   else ll = log((1-infprob)) - lambda  + cough_number_numeric*log(lambda) - lgamma(cough_number_numeric + 1);
   model cough_number_numeric ~ general(ll);
   predict lambda out = uk_cough_zero_infl_poisson;
run;
* This didn't work;



* Try using zero-inflated negative binomial approach with glimmix;

proc nlmixed data = UK_cough_imputation;
   parameters b0 = 0 b1 = 0 b2 = 0 b3 = 0
              a0 = 0 a1 = 0 alpha = 1;
   /* linear predictor for the inflation probability      */
   linpinfl = a0 + a1*subject_id_char;
   /* infprob = inflation probability for zeros           */
   /*         = logistic transform of the linear predictor*/
   infprob  = 1/(1+exp(-linpinfl));
   /* negative binomial with mean-dispersion */
   lambda   = exp(b0 + b1*cough + b2*X1 + b3*X2 + b4*X3 + b5*sex );
   /* Build the ZIP log likelihood */
   m = 1/alpha;
   p = 1/(1+alpha*lambda);
   if cough_number_numeric = 0 then 
        ll = log(infprob + (1-infprob)*(p**m));
   else ll = log(1-infprob) + log(gamma(m + cough_number_numeric)) - log(gamma(cough_number_numeric + 1))
   			 - log(gamma(m)) + m*log(p) + cough_number_numeric*log(1-p);
   model cough_number_numeric ~ general(ll);
run;
* This didn't work;




***** working with UMD data ******
* read in UMD data for tobit regrssion. will get predicted values for logfinal copies shedding,
*  which will be useful for predicting cough count in the instances where it was not observed. ;

%web_drop_table(WORK.UMD_144_flu);


FILENAME REFFILE 'C:\Users\jbueno\Downloads\UMD_144_flu_cases_for_tobit_then_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_144_flu;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_144_flu; RUN;



data UMD_144_flu; 
	set UMD_144_flu;
	subject_id_char = put(subject_id, 3.);
	study_day_char = put(dpo, 1.);
RUN;


data UMD_144_flu;
	set UMD_144_flu;
	
	if final_copies ~=. then
		logfinalcopies=log10(final_copies);

	if final_copies =. and type='A' then
		logfinalcopies=log10(2000);

	if final_copies =. and type='B' then
		logfinalcopies=log10(9000);

run;



data finetotal;
	set UMD_144_flu;

	if dpo=2 then
		X1=1;
	else
		X1=0;

	if dpo=3 then
		X2=1;
	else
		X2=0;
run;
quit;



proc genmod data = finetotal;
	class subject_id_char ;
	model logfinalcopies = X1 X2 /dist=poisson;
	repeated subject=subject_id_char/ printmle;
	ods OUTPUT parameterestimates=params;
run;

data Params;
	set Params (keep=Parameter Estimate);
run;

proc transpose data=Params out=Paramst;
run;

data _null_;
	set Paramst;
	call symput('unfBeta0', COL1);
	call symput('unfBeta1', COL2);
	call symput('unfBeta2', COL3);
run;

* with nested random effects;
* note that this take a long time to run and the SAS Studio times out before completion ;
* Thus, I'll add a version below without the nested random effect for now, but when I can get SAS desktop i'll try it.;
proc nlmixed data=finetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of fine Particles: Effect of cough_num dpo2 dpo3 SEX*cough BMI";
	parms sigma2_u=1 sigma2_u1=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1 beta2=&unfBeta2 beta3=&unfBeta3 
		beta4=&unfBeta4 beta5=&unfBeta5 beta6=&unfBeta6 beta7=&unfBeta7;
	bounds sigma2_u sigma2_u1 sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j+ b_1j + beta1*cough_number + beta2*X1 + beta3*X2 + 
		beta4*sex*cough_number +beta5*BMI + beta6*sex + beta7*age;

	if final_copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final_copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char out = umd_random_effect_subject ;
	random b_1j~ normal(0, sigma2_u1) subject=sample_id(subject_id_char) out = umd_random_effect_sample_wi_subject ;
	predict mu out = cough_model_finalcopies;
run;
quit;


proc nlmixed data=finetotal XTOL=1E-12 method=GAUSS qpoints=100;
	Title "Tobit Regression of fine Particles: Effect of dpo2 dpo3";
	parms sigma2_u=1 sigma2=1 beta0=&unfBeta0 beta1=&unfBeta1 beta2=&unfBeta2;
	bounds sigma2_u sigma2 >=0;
	pi=constant('pi');
	mu=beta0 + b_0j + beta1*X1 + beta2*X2;

	if final_copies ne . then
		ll=(1 / (sqrt(2*pi*sigma2))) * exp(-(logfinalcopies - mu)**2 / (2*sigma2) );

	if final_copies =.  then
		ll=probnorm((logfinalcopies - mu) / sqrt(sigma2) );
	L=log(ll);
	model logfinalcopies~ general(L);
	random b_0j ~ normal(0, sigma2_u) subject=subject_id_char out = umd_random_effects_no_nested ;
	predict mu out = umd_cough_single_random;
run;

quit;


* Now doing a mixed model with fixed effects of cough, loginalcopies, sex, age, BMI, dpo to predict umd population cough_number (need predicted values for those without cough);
FILENAME REFFILE 'C:\Users\jbueno\Downloads\UMD_data_for_cough_imputation.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.UMD_cough_imp_mixedmod_data;
	GETNAMES=YES;
RUN;

PROC CONTENTS DATA=WORK.UMD_cough_imp_mixedmod_data; RUN;



PROC MIXED DATA = Umd_cough_imp_mixedmod_data NOCLPRINT = 10 COVTEST method = reml;
	CLASS subject_id_char study_day_char sex;
	MODEL cough_number = cough Pred study_day_char age BMI sex /  S CHISQ influence residual outp = work.umd_cough_model_mixed ;
	RANDOM INTERCEPT / SUBJECT = subject_id_char TYPE = UN G GCORR V VCORR solution;
RUN;
quit;

